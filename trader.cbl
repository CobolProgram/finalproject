       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER.
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER2-FILE ASSIGN TO "trader2.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT TRADE-FILE ASSIGN TO "trade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER2-FILE.
       01  TRADER2-DETIALS.
           88 END-OF-FILE VALUE HIGH-VALUE.
           05 PROVINCE-ID    PIC   9(2).
           05 TRADER-ID      PIC   X(4). 
           05 INCOME         PIC   9(6).
       FD  TRADE-FILE.
       01  TRADE-DETIALS.
           05 PR-ID       PIC   9(2).
           05 SUMINCOME   PIC   9(10).
       WORKING-STORAGE SECTION. 
       01  SUM-PROVINCE   PIC   9(10).
       01  I PIC 9(2) VALUE 1  .
       01  COLUM-HEADING  PIC X(50) VALUE "PROVINCE     P INCOME".
       01  SUMARY   PIC   9(10) VALUE 0.
       01  MOST-ID  PIC   9(2).
       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT TRADER2-FILE 
           OPEN  OUTPUT TRADE-FILE 
           PERFORM UNTIL END-OF-FILE 
              READ  TRADER2-FILE 
                 AT END   SET   END-OF-FILE TO TRUE
              END-READ
              IF PROVINCE-ID EQUAL  I THEN
                 PERFORM 001-PROVINCE THRU 001-EXIT  
              END-IF 
              IF PROVINCE-ID NOT = I THEN
                 PERFORM 001-FIND-SUM
                 DISPLAY " I " I 
                 MOVE I TO PR-ID
                 MOVE SUM-PROVINCE TO SUMINCOME
                 WRITE TRADE-DETIALS 
                 DISPLAY SUM-PROVINCE " "
                 COMPUTE I = I + 1
              END-IF  
           END-PERFORM
           DISPLAY "SUM " SUMARY 
           DISPLAY "MOST ID" MOST-ID 
           CLOSE TRADER2-FILE 
           CLOSE TRADE-FILE 
           GOBACK 
           .
           
       001-PROVINCE.
                 COMPUTE SUM-PROVINCE = SUM-PROVINCE + INCOME 
              IF PROVINCE-ID NOT = I THEN 
                 COMPUTE SUM-PROVINCE = 0
              END-IF.
       001-EXIT.
           EXIT.
       
       001-FIND-SUM.
           IF SUM-PROVINCE > SUMARY THEN
              MOVE  SUM-PROVINCE TO SUMARY
              MOVE  PROVINCE-ID  TO MOST-ID
           END-IF 
           .
